from pathlib import Path

import pytest

from ktdg import Answer, Data, Question, Skill, Student, save_data


@pytest.fixture
def skills() -> list[Skill]:
    return [
        {"id": i, "difficulty": i * 0.25, "hash": str(i)} for i in range(5)
    ]


@pytest.fixture
def questions(skills: list[Skill]) -> list[Question]:
    return [
        {
            "id": i,
            "difficulty": i * 0.01,
            "slip": i * 0.01,
            "guess": i * 0.01,
            "skills": {
                j: (i + 1) * 0.1 + j * 0.01 for j in range(i % len(skills) + 1)
            },
            "hash": str(i),
        }
        for i in range(25)
    ]


@pytest.fixture
def students(skills: list[Skill]) -> list[Student]:
    return [
        {
            "id": i,
            "slip": i * 0.01,
            "guess": i * 0.01,
            "learning_rate": i * 0.01,
            "forget_rate": i * 0.01,
            "skills": {
                j: (i + 1) * 0.1 + j * 0.01 for j in range(i % len(skills) + 1)
            },
            "hash": str(i),
        }
        for i in range(5)
    ]


@pytest.fixture
def answers(
    students: list[Student], questions: list[Question]
) -> list[Answer]:
    return [
        {
            "id": i * len(questions) + j,
            "student": student["id"],
            "question": question["id"],
            "timestamp": j,
            "correct": True,
            "p_correct": 0.5,
            "state": {0: 0.5},
        }
        for i, student in enumerate(students)
        for j, question in enumerate(questions)
    ]


@pytest.fixture
def data(
    skills: list[Skill],
    students: list[Student],
    questions: list[Question],
    answers: list[Answer],
) -> Data:
    return {
        "skills": skills,
        "students": students,
        "questions": questions,
        "answers": answers,
    }


@pytest.fixture
def data_file(tmp_path: Path, data: Data) -> Path:
    path = tmp_path / "test_data.json"
    save_data(data, path, echo=False)
    return path
