from typing import Any

from pydantic import BaseModel


def equals(a: Any, b: Any, list_equiv_tuple: bool = False) -> bool:
    if type(a) != type(b) and (
        not list_equiv_tuple
        or isinstance(a, (tuple, list)) != isinstance(b, (tuple, list))
    ):
        return False

    if isinstance(a, (tuple, list)):
        if len(a) != len(b):
            return False
        else:
            return all(
                equals(aa, bb, list_equiv_tuple)
                for aa, bb in zip(a, b, strict=True)
            )

    elif isinstance(a, dict):
        try:
            return all(equals(a[k], b[k], list_equiv_tuple) for k in a.keys())
        except KeyError:
            print("d")
            print(a)
            print(b)
            return False

    elif isinstance(a, BaseModel):
        return equals(
            remove_key(a.dict(), ["seed", "hash"]),
            remove_key(b.dict(), ["seed", "hash"]),
            list_equiv_tuple,
        )

    else:
        if a != b:
            print("v")
            print(a)
            print(b)
        return a == b


def remove_key(dict_: dict[str, Any], key: str | list[str]) -> dict[str, Any]:
    if isinstance(key, str):
        key = [key]
    return {k: v for k, v in dict_.items() if k not in key}
