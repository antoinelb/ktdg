from typing import cast

import numpy as np
import pytest
import ruamel.yaml

from ktdg import Answer, Question, Skill, Student
from ktdg.answers import (
    _choose_question,
    _compute_p_correct,
    _compute_slip_and_guess,
    _generate_student_answers,
    _update_skill_state,
    add_comments,
    generate,
)
from ktdg.distributions import Normal
from ktdg.generation import Config
from ktdg.utils import create_skill_vector

############
# external #
############


def test_generate(
    config: Config,
    skills: list[Skill],
    students: list[Student],
    questions: list[Question],
) -> None:
    answers = generate(config.answers, students, questions, skills)
    assert len({a["id"] for a in answers}) == len(answers)


def test_add_comments(config: Config) -> None:
    config_ = add_comments(config.answers)
    assert isinstance(config_, ruamel.yaml.CommentedMap)
    assert len(config_.ca.items) == 8
    assert len(config_) == 8
    assert "wrong_answer_adjustment" in config_.ca.items
    assert "guess_adjustment" in config_.ca.items
    assert "mastery_importance" in config_.ca.items
    assert "n_per_student" in config_.ca.items
    assert "max_repetitions" in config_.ca.items
    assert "can_repeat_correct" in config_.ca.items
    assert "seed" in config_.ca.items
    assert "hash" in config_.ca.items
    assert "n_per_student" in config_


############
# internal #
############


def test_generate_student_answers(config: Config) -> None:
    rng = np.random.default_rng()

    student = cast(
        Student,
        {
            "id": 0,
            "slip": 0,
            "guess": 0,
            "learning_rate": 0.1,
            "forget_rate": 0,
            "skills": {},
        },
    )

    questions = [
        cast(
            Question,
            {
                "id": i,
                "difficulty": 0.5,
                "slip": 0,
                "guess": 0,
                "skills": {0: 1},
            },
        )
        for i in range(10)
    ]

    config.answers.n_per_student = Normal(mu=5, sigma=1)

    answers = _generate_student_answers(
        config.answers,
        student,
        questions,
        1,
        rng,
    )

    for i, (answer, prev_answer) in enumerate(
        zip(answers[1:], answers[:-1], strict=True)
    ):
        assert answer["student"] == student["id"]
        if i:
            assert answer["p_correct"] >= prev_answer["p_correct"]
        else:
            assert answer["p_correct"] > prev_answer["p_correct"]
        assert 0 <= answer["p_correct"] <= 1
        assert answer["timestamp"] > prev_answer["timestamp"]

    for _ in range(10):
        if len(
            _generate_student_answers(
                config.answers,
                student,
                questions,
                1,
                rng,
            )
        ) != len(answers):
            break
    else:
        assert 0


def test_choose_question(questions: list[Question]) -> None:
    rng = np.random.default_rng()

    question = _choose_question(
        [], questions, max_repetitions=1, can_repeat_correct=True, rng=rng
    )
    assert set(question.keys()) == set(Question.__annotations__.keys())

    answers = [
        cast(
            Answer,
            {
                "id": i,
                "student": 0,
                "question": q["id"],
                "timestamp": i,
                "correct": bool(i % 2),
                "p_correct": 0.5,
                "state": {},
            },
        )
        for i, q in enumerate(questions[: len(questions) // 2])
    ]

    ids = [question["id"] for question in questions[: len(questions) // 2]]
    questions_ = [
        _choose_question(
            answers,
            questions,
            max_repetitions=1,
            can_repeat_correct=True,
            rng=rng,
        )
        for _ in range(len(questions) * 10)
    ]
    assert all(question["id"] not in ids for question in questions_)

    ids = [question["id"] for question in questions]
    questions_ = [
        _choose_question(
            answers,
            questions,
            max_repetitions=2,
            can_repeat_correct=True,
            rng=rng,
        )
        for _ in range(len(questions) * 10)
    ]
    ids_ = [question["id"] for question in questions_]
    assert all(id_ in ids_ for id_ in ids)

    ids = [
        question["id"]
        for i, question in enumerate(questions[: len(questions) // 2])
        if i % 2
    ]
    questions_ = [
        _choose_question(
            answers,
            questions,
            max_repetitions=2,
            can_repeat_correct=False,
            rng=rng,
        )
        for _ in range(len(questions) * 10)
    ]
    assert all(question["id"] not in ids for question in questions_)

    with pytest.raises(RuntimeError):
        _choose_question(
            answers,
            questions[: len(questions) // 2],
            max_repetitions=1,
            can_repeat_correct=True,
            rng=rng,
        )


def test_choose_question__no_max_repetition(questions: list[Question]) -> None:
    rng = np.random.default_rng()

    answers = [
        Answer(
            id=i,
            student=0,
            question=questions[0]["id"],
            timestamp=i,
            correct=bool(i % 2),
            p_correct=0.5,
            state={},
        )
        for i in range(5)
    ]

    questions_ = [
        _choose_question(
            answers,
            questions[:1],
            max_repetitions=0,
            can_repeat_correct=True,
            rng=rng,
        )
        for _ in range(20)
    ]
    assert all(q["id"] == questions[0]["id"] for q in questions_)


def test_compute_p_correct__only_skills(
    skills: list[Skill], students: list[Student], questions: list[Question]
) -> None:
    student = students[0]
    question = questions[0]

    student["guess"] = 0
    student["slip"] = 0
    question["guess"] = 0
    question["slip"] = 0
    question["difficulty"] = 0
    mastery_importance = 1

    student["skills"] = {skill["id"]: 0.5 for skill in skills}
    question["skills"] = {skill["id"]: 1 for skill in skills}
    student_skills = create_skill_vector(student["skills"], len(skills))
    question_skills = create_skill_vector(question["skills"], len(skills))
    regular_result = _compute_p_correct(
        student_skills, question_skills, student, question, mastery_importance
    )

    student["skills"] = {skill["id"]: 1 for skill in skills}
    question["skills"] = {skill["id"]: 1 for skill in skills}
    student_skills = create_skill_vector(student["skills"], len(skills))
    question_skills = create_skill_vector(question["skills"], len(skills))
    easy_result = _compute_p_correct(
        student_skills, question_skills, student, question, mastery_importance
    )

    student["skills"] = {skill["id"]: 0 for skill in skills}
    question["skills"] = {skill["id"]: 1 for skill in skills}
    student_skills = create_skill_vector(student["skills"], len(skills))
    question_skills = create_skill_vector(question["skills"], len(skills))
    hard_result = _compute_p_correct(
        student_skills, question_skills, student, question, mastery_importance
    )

    assert 0 <= hard_result < regular_result < easy_result <= 1


def test_compute_p_correct__difficulty(
    skills: list[Skill], students: list[Student], questions: list[Question]
) -> None:
    student = students[0]
    question = questions[0]

    student["guess"] = 0
    student["slip"] = 0
    question["guess"] = 0
    question["slip"] = 0
    mastery_importance = 1
    student_skills = create_skill_vector(student["skills"], len(skills))
    question_skills = create_skill_vector(question["skills"], len(skills))

    question["difficulty"] = 0
    easy_result = _compute_p_correct(
        student_skills, question_skills, student, question, mastery_importance
    )

    question["difficulty"] = 1
    hard_result = _compute_p_correct(
        student_skills, question_skills, student, question, mastery_importance
    )
    assert 0 <= hard_result < easy_result <= 1


def test_compute_p_correct__slip_and_guess(
    skills: list[Skill], students: list[Student], questions: list[Question]
) -> None:
    student = students[0]
    question = questions[0]

    question["difficulty"] = 0
    mastery_importance = 1
    student_skills = create_skill_vector(student["skills"], len(skills))
    question_skills = create_skill_vector(question["skills"], len(skills))

    student["guess"] = 0
    student["slip"] = 0
    question["guess"] = 0
    question["slip"] = 0
    regular_result = _compute_p_correct(
        student_skills, question_skills, student, question, mastery_importance
    )

    student["guess"] = 0.25
    student["slip"] = 0
    question["guess"] = 0
    question["slip"] = 0
    cheater_result = _compute_p_correct(
        student_skills, question_skills, student, question, mastery_importance
    )
    assert 0 <= regular_result < cheater_result <= 1

    student["guess"] = 0
    student["slip"] = 0.25
    question["guess"] = 0
    question["slip"] = 0
    nervous_result = _compute_p_correct(
        student_skills, question_skills, student, question, mastery_importance
    )
    assert 0 <= nervous_result < regular_result <= 1

    student["guess"] = 0
    student["slip"] = 0
    question["guess"] = 0.25
    question["slip"] = 0
    easy_result = _compute_p_correct(
        student_skills, question_skills, student, question, mastery_importance
    )
    assert 0 <= regular_result < easy_result <= 1

    student["guess"] = 0
    student["slip"] = 0
    question["guess"] = 0
    question["slip"] = 0.25
    ambiguous_result = _compute_p_correct(
        student_skills, question_skills, student, question, mastery_importance
    )
    assert 0 <= ambiguous_result < regular_result <= 1

    student["guess"] = 0.25
    student["slip"] = 0
    question["guess"] = 0.2
    question["slip"] = 0
    different_guesses_result = _compute_p_correct(
        student_skills, question_skills, student, question, mastery_importance
    )
    assert 0 <= cheater_result < different_guesses_result <= 1

    student["guess"] = 0
    student["slip"] = 0.25
    question["guess"] = 0
    question["slip"] = 0.2
    different_slips_result = _compute_p_correct(
        student_skills, question_skills, student, question, mastery_importance
    )
    assert 0 <= different_slips_result < nervous_result <= 1


def test_compute_p_correct__mastery_importance(
    skills: list[Skill], students: list[Student], questions: list[Question]
) -> None:
    student = students[0]
    question = questions[0]

    student["guess"] = 0.25
    student["slip"] = 0.25
    question["guess"] = 0.25
    question["slip"] = 0.25
    student_skills = create_skill_vector(student["skills"], len(skills))
    question_skills = create_skill_vector(question["skills"], len(skills))

    mastery_importance = 1.0
    question["difficulty"] = 0
    regular_easy_result = _compute_p_correct(
        student_skills, question_skills, student, question, mastery_importance
    )

    mastery_importance = 10
    no_chance_easy_result = _compute_p_correct(
        student_skills, question_skills, student, question, mastery_importance
    )

    mastery_importance = 0.1
    chance_easy_result = _compute_p_correct(
        student_skills, question_skills, student, question, mastery_importance
    )

    assert (
        0
        <= chance_easy_result
        < regular_easy_result
        < no_chance_easy_result
        <= 1
    )

    mastery_importance = 1
    question["difficulty"] = 3
    regular_hard_result = _compute_p_correct(
        student_skills, question_skills, student, question, mastery_importance
    )

    mastery_importance = 10
    no_chance_hard_result = _compute_p_correct(
        student_skills, question_skills, student, question, mastery_importance
    )

    mastery_importance = 0.1
    chance_hard_result = _compute_p_correct(
        student_skills, question_skills, student, question, mastery_importance
    )

    assert (
        0
        <= no_chance_hard_result
        < regular_hard_result
        < chance_hard_result
        <= 1
    )


def test_update_skill_state__no_modifiers(
    skills: list[Skill], students: list[Student], questions: list[Question]
) -> None:
    student = students[0]
    question = questions[0]
    question["skills"] = {skill["id"]: 1 for skill in skills[::2]}
    unlearned_skills = {
        skill["id"]
        for skill in skills
        if skill["id"] not in question["skills"]
    }

    student["forget_rate"] = 0
    student["learning_rate"] = 0.5
    question["difficulty"] = 0.5
    wrong_answer_adjustment = 0
    guess_adjustment = 0
    correct = False

    skill_state = create_skill_vector({}, len(skills))
    question_skills = create_skill_vector(question["skills"], len(skills))

    skill_state = _update_skill_state(
        skill_state,
        question_skills,
        correct,
        student,
        question,
        wrong_answer_adjustment,
        guess_adjustment,
    )
    for skill in question["skills"]:
        assert skill_state[skill] == 0.5
    for skill in unlearned_skills:
        assert skill_state[skill] == 0

    skill_state = _update_skill_state(
        skill_state,
        question_skills,
        correct,
        student,
        question,
        wrong_answer_adjustment,
        guess_adjustment,
    )
    for skill in question["skills"]:
        assert skill_state[skill] == 1
    for skill in unlearned_skills:
        assert skill_state[skill] == 0

    skill_state = _update_skill_state(
        skill_state,
        question_skills,
        correct,
        student,
        question,
        wrong_answer_adjustment,
        guess_adjustment,
    )
    for skill in question["skills"]:
        assert skill_state[skill] == 1
    for skill in unlearned_skills:
        assert skill_state[skill] == 0


def test_update_skill_state__forget_rate(
    skills: list[Skill], students: list[Student], questions: list[Question]
) -> None:
    # preparation
    student = students[0]
    question = questions[0]
    question["skills"] = {skill["id"]: 1 for skill in skills[::2]}
    unlearned_skills = {
        skill["id"]
        for skill in skills
        if skill["id"] not in question["skills"]
    }

    student["learning_rate"] = 0.5
    question["difficulty"] = 0.5
    wrong_answer_adjustment = 0
    guess_adjustment = 0
    correct = False
    question_skills = create_skill_vector(question["skills"], len(skills))

    student["forget_rate"] = 0.25

    skill_state = create_skill_vector({}, len(skills))

    skill_state = _update_skill_state(
        skill_state,
        question_skills,
        correct,
        student,
        question,
        wrong_answer_adjustment,
        guess_adjustment,
    )
    for skill in question["skills"]:
        assert skill_state[skill] == 0.5
    for skill in unlearned_skills:
        assert skill_state[skill] == 0

    skill_state = _update_skill_state(
        skill_state,
        question_skills,
        correct,
        student,
        question,
        wrong_answer_adjustment,
        guess_adjustment,
    )
    for skill in question["skills"]:
        assert skill_state[skill] < 1
    for skill in unlearned_skills:
        assert skill_state[skill] == 0


def test_update_skill_state__question_difficulty(
    skills: list[Skill], students: list[Student], questions: list[Question]
) -> None:
    # preparation
    student = students[0]
    question = questions[0]
    question["skills"] = {skill["id"]: 1 for skill in skills[::2]}
    unlearned_skills = {
        skill["id"]
        for skill in skills
        if skill["id"] not in question["skills"]
    }

    student["forget_rate"] = 0
    wrong_answer_adjustment = 0
    guess_adjustment = 0
    correct = False
    student["learning_rate"] = 0.25
    question_skills = create_skill_vector(question["skills"], len(skills))

    # first test with an easy question
    question["difficulty"] = 0
    skill_state = create_skill_vector({}, len(skills))
    skill_state = _update_skill_state(
        skill_state,
        question_skills,
        correct,
        student,
        question,
        wrong_answer_adjustment,
        guess_adjustment,
    )
    for skill in question["skills"]:
        assert skill_state[skill] < student["learning_rate"]
    for skill in unlearned_skills:
        assert skill_state[skill] == 0

    # second test with an average question
    question["difficulty"] = 0.5
    skill_state = create_skill_vector({}, len(skills))
    skill_state = _update_skill_state(
        skill_state,
        question_skills,
        correct,
        student,
        question,
        wrong_answer_adjustment,
        guess_adjustment,
    )
    for skill in question["skills"]:
        assert skill_state[skill] == student["learning_rate"]
    for skill in unlearned_skills:
        assert skill_state[skill] == 0

    # third test with a hard question
    question["difficulty"] = 1
    skill_state = create_skill_vector({}, len(skills))
    skill_state = _update_skill_state(
        skill_state,
        question_skills,
        correct,
        student,
        question,
        wrong_answer_adjustment,
        guess_adjustment,
    )
    for skill in question["skills"]:
        assert skill_state[skill] > student["learning_rate"]
    for skill in unlearned_skills:
        assert skill_state[skill] == 0


def test_update_skill_state__wrong_answer_adjustment(
    skills: list[Skill], students: list[Student], questions: list[Question]
) -> None:
    # preparation
    student = students[0]
    question = questions[0]
    question["skills"] = {skill["id"]: 1 for skill in skills[::2]}
    unlearned_skills = {
        skill["id"]
        for skill in skills
        if skill["id"] not in question["skills"]
    }

    student["forget_rate"] = 0
    question["difficulty"] = 0.5
    student["learning_rate"] = 0.25
    question_skills = create_skill_vector(question["skills"], len(skills))
    guess_adjustment = 0

    # first test with a correct answer and no adjustment
    wrong_answer_adjustment = 0.0
    correct = True
    skill_state = create_skill_vector({}, len(skills))
    skill_state = _update_skill_state(
        skill_state,
        question_skills,
        correct,
        student,
        question,
        wrong_answer_adjustment,
        guess_adjustment,
    )
    for skill in question["skills"]:
        assert skill_state[skill] == student["learning_rate"]
    for skill in unlearned_skills:
        assert skill_state[skill] == 0

    # second test with a correct answer and an adjustment
    wrong_answer_adjustment = 0.5
    correct = True
    skill_state = create_skill_vector({}, len(skills))
    skill_state = _update_skill_state(
        skill_state,
        question_skills,
        correct,
        student,
        question,
        wrong_answer_adjustment,
        guess_adjustment,
    )
    for skill in question["skills"]:
        assert skill_state[skill] == student["learning_rate"]
    for skill in unlearned_skills:
        assert skill_state[skill] == 0

    # third test with an incorrect answer and no adjustment
    wrong_answer_adjustment = 0
    correct = False
    skill_state = create_skill_vector({}, len(skills))
    skill_state = _update_skill_state(
        skill_state,
        question_skills,
        correct,
        student,
        question,
        wrong_answer_adjustment,
        guess_adjustment,
    )
    for skill in question["skills"]:
        assert skill_state[skill] == student["learning_rate"]
    for skill in unlearned_skills:
        assert skill_state[skill] == 0

    # third test with an incorrect answer and an adjustment
    wrong_answer_adjustment = 0.5
    correct = False
    skill_state = create_skill_vector({}, len(skills))
    skill_state = _update_skill_state(
        skill_state,
        question_skills,
        correct,
        student,
        question,
        wrong_answer_adjustment,
        guess_adjustment,
    )
    for skill in question["skills"]:
        assert skill_state[skill] < student["learning_rate"]
    for skill in unlearned_skills:
        assert skill_state[skill] == 0


def test_update_skill_state__guess_adjustment(
    skills: list[Skill], students: list[Student], questions: list[Question]
) -> None:
    # preparation
    student = students[0]
    question = questions[0]
    question["skills"] = {skill["id"]: 1 for skill in skills[::2]}
    unlearned_skills = {
        skill["id"]
        for skill in skills
        if skill["id"] not in question["skills"]
    }

    student["forget_rate"] = 0
    question["difficulty"] = 0.5
    student["learning_rate"] = 0.25
    question_skills = create_skill_vector(question["skills"], len(skills))
    wrong_answer_adjustment = 0

    # first test with a correct answer and no adjustment
    guess_adjustment = 0.0
    correct = True
    skill_state = create_skill_vector({}, len(skills))
    skill_state = _update_skill_state(
        skill_state,
        question_skills,
        correct,
        student,
        question,
        wrong_answer_adjustment,
        guess_adjustment,
    )
    for skill in question["skills"]:
        assert skill_state[skill] == student["learning_rate"]
    for skill in unlearned_skills:
        assert skill_state[skill] == 0

    # second test with an adjustment
    guess_adjustment = 0.25
    correct = True
    skill_state = create_skill_vector({}, len(skills))
    skill_state = _update_skill_state(
        skill_state,
        question_skills,
        correct,
        student,
        question,
        wrong_answer_adjustment,
        guess_adjustment,
    )
    for skill in question["skills"]:
        assert skill_state[skill] < student["learning_rate"]
    for skill in unlearned_skills:
        assert skill_state[skill] == 0


def test_compute_slip_and_guess() -> None:
    student = cast(
        Student,
        {
            "id": 0,
            "slip": 0.1,
            "guess": 0.2,
            "learning_rate": 0,
            "forget_rate": 0,
            "skills": {},
        },
    )
    question = cast(
        Question,
        {
            "id": 0,
            "difficulty": 0,
            "slip": 0.05,
            "guess": 0.15,
            "skills": {},
        },
    )

    correct_slip = 1 - (0.9 * 0.95) ** 0.5
    correct_guess = 1 - (0.8 * 0.85) ** 0.5
    slip, guess = _compute_slip_and_guess(student, question)
    assert slip == correct_slip
    assert guess == correct_guess
