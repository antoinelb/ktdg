from typing import Type

import numpy as np
import pytest
import ruamel.yaml

from ktdg.distributions import (
    Binomial,
    Constant,
    Distribution,
    Normal,
    add_binomial_comments,
    add_constant_comments,
    add_distribution_comments,
    add_normal_comments,
    distributions,
    generate_binomial,
    generate_constant,
    generate_normal,
    generate_values,
    parse_distribution,
)

#########
# types #
#########


@pytest.mark.parametrize("distribution", distributions)
def test_types_set(distribution: str) -> None:
    assert get_distribution(distribution)().type == distribution


############
# external #
############


@pytest.mark.parametrize("distribution", distributions)
def test_parse_distribution(distribution: str) -> None:
    assert isinstance(
        parse_distribution({"type": distribution}),
        get_distribution(distribution),
    )


def test_parse_distribution__wrong() -> None:
    with pytest.raises(ValueError):
        parse_distribution({})
    with pytest.raises(ValueError):
        parse_distribution(None)
    with pytest.raises(NotImplementedError):
        parse_distribution({"type": "wrong"})


@pytest.mark.parametrize("distribution", distributions)
def test_generate_values(distribution: str) -> None:
    rng = np.random.default_rng()
    values = generate_values(25, get_distribution(distribution)(), rng)
    assert isinstance(values, list) and len(values) == 25


def test_generate_values__wrong() -> None:
    rng = np.random.default_rng()
    with pytest.raises(NotImplementedError):
        generate_values(25, Distribution(type="constant"), rng)


@pytest.mark.parametrize("distribution", distributions)
def test_add_distribution_comments(distribution: str) -> None:
    config_ = add_distribution_comments(get_distribution(distribution)())
    assert isinstance(config_, ruamel.yaml.CommentedMap)
    assert "type" in config_.ca.items


def test_add_distribution_comments__wrong() -> None:
    with pytest.raises(NotImplementedError):
        add_distribution_comments(Distribution(type="constant"))


############
# internal #
############


def test_generate_constant() -> None:
    rng = np.random.default_rng()

    distribution = Constant(value=5)
    values = generate_constant(25, distribution, rng)
    assert all(val == 5 for val in values)

    distribution = Constant(value=-5)
    values = generate_constant(25, distribution, rng)
    assert all(val == -5 for val in values)


@pytest.mark.flaky(reruns=3)
def test_generate_normal() -> None:
    rng = np.random.default_rng()

    distribution = Normal(mu=1, sigma=5)
    values = generate_normal(10_000, distribution, rng)
    assert (
        0.67 * len(values)
        < len([v for v in values if -4 < v < 6])
        < 0.69 * len(values)
    )
    assert (
        0.94 * len(values)
        < len([v for v in values if -9 < v < 11])
        < 0.96 * len(values)
    )
    assert (
        0.99 * len(values)
        < len([v for v in values if -14 < v < 16])
        <= 1 * len(values)
    )


def test_generate_binomial() -> None:
    rng = np.random.default_rng()

    distribution = Binomial(n=2, p=0.4)
    values = generate_binomial(100_000, distribution, rng)
    assert (
        abs(
            len([v for v in values if v == 0]) / 100_000
            - 2 / (2 * 1) * 0.4**0 * 0.6**2
        )
        < 0.01
    )
    assert (
        abs(
            len([v for v in values if v == 1]) / 100_000
            - 2 / (1 * 1) * 0.4**1 * 0.6**1
        )
        < 0.01
    )
    assert (
        abs(
            len([v for v in values if v == 2]) / 100_000
            - 2 / (1 * 2) * 0.4**2 * 0.6**0
        )
        < 0.01
    )


def test_add_constant_comments() -> None:
    config_ = add_constant_comments(
        ruamel.yaml.CommentedMap(Constant().dict())
    )
    assert len(config_.ca.items) == 1
    assert "value" in config_.ca.items


def test_add_normal_comments() -> None:
    config_ = add_normal_comments(ruamel.yaml.CommentedMap(Normal().dict()))
    assert len(config_.ca.items) == 2
    assert "mu" in config_.ca.items
    assert "sigma" in config_.ca.items


def test_add_binomial_comments() -> None:
    config_ = add_binomial_comments(
        ruamel.yaml.CommentedMap(Binomial().dict())
    )
    assert len(config_.ca.items) == 2
    assert "n" in config_.ca.items
    assert "p" in config_.ca.items


#########
# utils #
#########


def get_distribution(distribution: str) -> Type[Distribution]:
    if distribution == "constant":
        return Constant
    elif distribution == "normal":
        return Normal
    elif distribution == "binomial":
        return Binomial
    else:
        raise NotImplementedError()
